pub fn main() {
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(
            &[
                "protos/app-manager.proto",
            ],
            &["protos"],
        )
        .unwrap();
}
