FROM rust:1.75.0 as rust-builder

RUN cargo install cargo-chef

FROM rust-builder as planner

WORKDIR /app
COPY . /app

RUN cargo chef prepare --recipe-path recipe.json

FROM rust-builder as build-env

RUN apt update && apt install -y libssl-dev pkg-config build-essential cmake protobuf-compiler

WORKDIR /app

COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --features __prisma_cli --release --recipe-path recipe.json

COPY . /app

RUN cargo prisma generate

RUN cargo build --release --bin=download-new-apps

FROM ubuntu:22.04

COPY --from=build-env /app/target/release/download-new-apps /

CMD ["/download-new-apps"]
