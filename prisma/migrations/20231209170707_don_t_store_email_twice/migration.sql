/*
  Warnings:

  - You are about to drop the column `email` on the `AcmeAccount` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "AcmeAccount" DROP COLUMN "email",
ADD COLUMN     "shareEmail" BOOLEAN NOT NULL DEFAULT false;
