/*
  Warnings:

  - The primary key for the `AppDomain` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Domain` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - A unique constraint covering the columns `[id]` on the table `AppDomain` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[id]` on the table `Domain` will be added. If there are existing duplicate values, this will fail.
  - The required column `id` was added to the `AppDomain` table with a prisma-level default value. This is not possible if the table is not empty. Please add this column as optional, then populate it before making it required.
  - The required column `id` was added to the `Domain` table with a prisma-level default value. This is not possible if the table is not empty. Please add this column as optional, then populate it before making it required.

*/
-- DropForeignKey
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_parentDomainName_fkey";

-- DropIndex
DROP INDEX "AppDomain_name_key";

-- DropIndex
DROP INDEX "Domain_name_key";

-- AlterTable
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_pkey",
ADD COLUMN     "id" TEXT NOT NULL,
ADD CONSTRAINT "AppDomain_pkey" PRIMARY KEY ("id");

-- AlterTable
ALTER TABLE "Domain" DROP CONSTRAINT "Domain_pkey",
ADD COLUMN     "id" TEXT NOT NULL,
ADD CONSTRAINT "Domain_pkey" PRIMARY KEY ("id");

-- CreateIndex
CREATE UNIQUE INDEX "AppDomain_id_key" ON "AppDomain"("id");

-- CreateIndex
CREATE UNIQUE INDEX "Domain_id_key" ON "Domain"("id");

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_parentDomainName_fkey" FOREIGN KEY ("parentDomainName") REFERENCES "Domain"("id") ON DELETE SET NULL ON UPDATE CASCADE;
