/*
  Warnings:

  - Changed the type of `provider` on the `Domain` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- CreateEnum
CREATE TYPE "DNSProvider" AS ENUM ('CLOUDFLARE', 'NONE');

-- AlterTable
ALTER TABLE "Domain" DROP COLUMN "provider",
ADD COLUMN     "provider" "DNSProvider" NOT NULL;
