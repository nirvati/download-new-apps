/*
  Warnings:

  - You are about to drop the column `ownerId` on the `AcmeAccount` table. All the data in the column will be lost.
  - You are about to drop the column `appId` on the `AppDomain` table. All the data in the column will be lost.
  - You are about to drop the column `ownerId` on the `AppDomain` table. All the data in the column will be lost.
  - You are about to drop the column `name` on the `AppInstallation` table. All the data in the column will be lost.
  - You are about to drop the column `ownerId` on the `AppInstallation` table. All the data in the column will be lost.
  - You are about to drop the column `ownerId` on the `Domain` table. All the data in the column will be lost.
  - The primary key for the `User` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `roles` on the `User` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[provider,ownerName]` on the table `AcmeAccount` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[name]` on the table `AppDomain` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[app_id,ownerName]` on the table `AppInstallation` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[name,ownerName]` on the table `Domain` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[username]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `ownerName` to the `AcmeAccount` table without a default value. This is not possible if the table is not empty.
  - Made the column `appInstallationId` on table `AppDomain` required. This step will fail if there are existing NULL values in that column.
  - Added the required column `app_id` to the `AppInstallation` table without a default value. This is not possible if the table is not empty.
  - Added the required column `ownerName` to the `AppInstallation` table without a default value. This is not possible if the table is not empty.
  - Added the required column `ownerName` to the `Domain` table without a default value. This is not possible if the table is not empty.
  - Added the required column `role` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `userGroupId` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `username` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "WebAuthnCredentialType" AS ENUM ('PASSKEY', 'SECOND_FACTOR');

-- AlterEnum
ALTER TYPE "Role" ADD VALUE 'INSTANCE_ADMIN';

-- DropForeignKey
ALTER TABLE "AcmeAccount" DROP CONSTRAINT "AcmeAccount_ownerId_fkey";

-- DropForeignKey
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_appInstallationId_fkey";

-- DropForeignKey
ALTER TABLE "AppDomain" DROP CONSTRAINT "AppDomain_ownerId_fkey";

-- DropForeignKey
ALTER TABLE "AppInstallation" DROP CONSTRAINT "AppInstallation_ownerId_fkey";

-- DropForeignKey
ALTER TABLE "Domain" DROP CONSTRAINT "Domain_ownerId_fkey";

-- DropIndex
DROP INDEX "AcmeAccount_provider_ownerId_key";

-- DropIndex
DROP INDEX "AppDomain_name_ownerId_key";

-- DropIndex
DROP INDEX "AppInstallation_name_ownerId_key";

-- DropIndex
DROP INDEX "Domain_name_ownerId_key";

-- DropIndex
DROP INDEX "User_id_key";

-- AlterTable
ALTER TABLE "AcmeAccount" DROP COLUMN "ownerId",
ADD COLUMN     "ownerName" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "AppDomain" DROP COLUMN "appId",
DROP COLUMN "ownerId",
ALTER COLUMN "appInstallationId" SET NOT NULL;

-- AlterTable
ALTER TABLE "AppInstallation" DROP COLUMN "name",
DROP COLUMN "ownerId",
ADD COLUMN     "app_id" TEXT NOT NULL,
ADD COLUMN     "ownerName" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Domain" DROP COLUMN "ownerId",
ADD COLUMN     "ownerName" TEXT NOT NULL,
ALTER COLUMN "providerAuth" DROP NOT NULL;

-- AlterTable
ALTER TABLE "User" DROP CONSTRAINT "User_pkey",
DROP COLUMN "id",
DROP COLUMN "roles",
ADD COLUMN     "role" "Role" NOT NULL,
ADD COLUMN     "userGroupId" TEXT NOT NULL,
ADD COLUMN     "username" TEXT NOT NULL,
ADD CONSTRAINT "User_pkey" PRIMARY KEY ("username");

-- CreateTable
CREATE TABLE "WebAuthnCredential" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ownerName" TEXT NOT NULL,
    "credentialId" BYTEA NOT NULL,
    "publicKey" BYTEA NOT NULL,
    "signCount" INTEGER NOT NULL,

    CONSTRAINT "WebAuthnCredential_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "UserGroup" (
    "name" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "UserGroup_pkey" PRIMARY KEY ("name")
);

-- CreateIndex
CREATE UNIQUE INDEX "WebAuthnCredential_id_key" ON "WebAuthnCredential"("id");

-- CreateIndex
CREATE UNIQUE INDEX "WebAuthnCredential_credentialId_key" ON "WebAuthnCredential"("credentialId");

-- CreateIndex
CREATE UNIQUE INDEX "UserGroup_name_key" ON "UserGroup"("name");

-- CreateIndex
CREATE UNIQUE INDEX "AcmeAccount_provider_ownerName_key" ON "AcmeAccount"("provider", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "AppDomain_name_key" ON "AppDomain"("name");

-- CreateIndex
CREATE UNIQUE INDEX "AppInstallation_app_id_ownerName_key" ON "AppInstallation"("app_id", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "Domain_name_ownerName_key" ON "Domain"("name", "ownerName");

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- AddForeignKey
ALTER TABLE "AcmeAccount" ADD CONSTRAINT "AcmeAccount_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WebAuthnCredential" ADD CONSTRAINT "WebAuthnCredential_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "User" ADD CONSTRAINT "User_userGroupId_fkey" FOREIGN KEY ("userGroupId") REFERENCES "UserGroup"("name") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Domain" ADD CONSTRAINT "Domain_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppDomain" ADD CONSTRAINT "AppDomain_appInstallationId_fkey" FOREIGN KEY ("appInstallationId") REFERENCES "AppInstallation"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "AppInstallation" ADD CONSTRAINT "AppInstallation_ownerName_fkey" FOREIGN KEY ("ownerName") REFERENCES "User"("username") ON DELETE RESTRICT ON UPDATE CASCADE;
