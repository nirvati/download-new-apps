use crate::api::download_apps_request::Reference;
use crate::api::manager_client::ManagerClient;
use crate::api::{DownloadAppsRequest, GenerateFilesRequest, StoreRef, UserState};
use crate::prisma::user;
use prisma_client_rust::serde_json;
use std::collections::HashMap;
use tonic::Request;

#[allow(unused, warnings)]
mod prisma;

mod api {
    tonic::include_proto!("apps");
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let db_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let apps_endpoint = std::env::var("APPS_ENDPOINT").expect("APPS_ENDPOINT must be set");
    let endpoint = tonic::transport::Endpoint::from_shared(apps_endpoint)
        .expect("Failed to create apps endpoint");
    let channel = endpoint.connect().await.expect("Failed to connect to core");
    let db = prisma::new_client_with_url(&db_url)
        .await
        .expect("Failed to create database client");
    let mut app_manager = ManagerClient::new(channel);
    let users = db
        .user()
        .find_many(vec![])
        .select(user::select!({
            username
            app_installation: select {
                app_id
                settings
            }
        }))
        .exec()
        .await
        .expect("Failed to find users");
    for user in users {
        let app_settings: HashMap<String, String> = user
            .app_installation
            .into_iter()
            .map(|app| (app.app_id, serde_json::to_string(&app.settings).unwrap()))
            .collect();
        let apps = app_settings.keys().cloned().collect();
        let user_state = UserState {
            user: user.username,
            installed_apps: apps,
            app_settings,
        };
        app_manager
            .download(Request::new(DownloadAppsRequest {
                user_state: Some(user_state.clone()),
                reference: Some(Reference::Store(StoreRef {
                    new_or_not_installed_only: true,
                    details: None,
                })),
            }))
            .await
            .expect("Failed to download apps");
        app_manager
            .generate_files(Request::new(GenerateFilesRequest {
                user_state: Some(user_state.clone()),
            }))
            .await
            .expect("Failed to generate files");
    }
}
